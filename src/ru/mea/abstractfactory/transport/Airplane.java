package ru.mea.abstractfactory.transport;

public interface Airplane {
    void flight();
}
