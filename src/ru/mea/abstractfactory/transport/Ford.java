package ru.mea.abstractfactory.transport;

public class Ford implements Car {
    @Override
    public void drive() {
        System.out.println("Форд едет");
    }

    @Override
    public void stop() {
        System.out.println("Форд остановился");
    }
}
