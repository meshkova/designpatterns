package ru.mea.abstractfactory.transport;

public class Boeing747 implements Airplane {
    @Override
    public void flight() {
        System.out.println("Боинг 747 летит");
    }
}
