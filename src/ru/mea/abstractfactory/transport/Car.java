package ru.mea.abstractfactory.transport;

public interface Car {
    void drive();

    void stop();
}
