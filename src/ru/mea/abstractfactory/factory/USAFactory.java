package ru.mea.abstractfactory.factory;


import ru.mea.abstractfactory.transport.Airplane;
import ru.mea.abstractfactory.transport.Boeing747;
import ru.mea.abstractfactory.transport.Car;
import ru.mea.abstractfactory.transport.Ford;

public class USAFactory implements TransportFactory {
    @Override
    public Car createCar() {
        return new Ford();
    }

    @Override
    public Airplane createAirplane() {
        return new Boeing747();
    }
}
