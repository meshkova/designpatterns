package ru.mea.abstractfactory.factory;


import ru.mea.abstractfactory.transport.Airplane;
import ru.mea.abstractfactory.transport.Car;
import ru.mea.abstractfactory.transport.Niva;
import ru.mea.abstractfactory.transport.TU204;

public class RussianFactory  implements TransportFactory {
    @Override
    public Car createCar() {
        return new Niva();
    }

    @Override
    public Airplane createAirplane() {
        return new TU204();
    }
}
