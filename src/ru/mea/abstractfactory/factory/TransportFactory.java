package ru.mea.abstractfactory.factory;


import ru.mea.abstractfactory.transport.Airplane;
import ru.mea.abstractfactory.transport.Car;

public interface TransportFactory {
    Car createCar();

    Airplane createAirplane();
}
