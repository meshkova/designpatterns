package ru.mea.abstractfactory;


import ru.mea.abstractfactory.factory.RussianFactory;
import ru.mea.abstractfactory.factory.TransportFactory;
import ru.mea.abstractfactory.factory.USAFactory;
import ru.mea.abstractfactory.transport.Airplane;
import ru.mea.abstractfactory.transport.Car;


import java.util.Random;

public class Demo {
    public static void main(String[] args) {
        Random random = new Random();
        boolean flag = random.nextBoolean();
        TransportFactory transportFactory;
        if (flag) {
            transportFactory = new RussianFactory();
        } else {
            transportFactory = new USAFactory();
        }
       
        Car car = transportFactory.createCar();
        car.drive();
        car.stop();

        Airplane airplane = transportFactory.createAirplane();
        airplane.flight();

    }
}
