package ru.mea.factorymethod;

public class CarSelector {
    /**
     * Фабричный метод, который создает нужный автомобиль
     *
     * @param roadType тип дороги
     * @return автомобиль в зависимости типа дороги
     */
    Car getCar(RoadType roadType) {
        Car car = null;
        switch (roadType) {
            case CITY:
                car = new Renault();
                break;
            case OFF_ROADS:
                car = new Geep();
                break;
            case TRACK:
                car = new Ferrari();
                break;
        }
        return car;
    }
}
