package ru.mea.factorymethod;

public class Ferrari implements Car {
    @Override
    public void drive() {
        System.out.println("Едет со скоростью 300 км/ч");
    }

    @Override
    public void stop() {
        System.out.println("Останавливается за 20 сек.");
    }
}
