package ru.mea.factorymethod;

public enum RoadType {
    CITY,
    OFF_ROADS,
    TRACK
}
