package ru.mea.factorymethod;

public class Demo {
    public static void main(String[] args) {
        CarSelector carSelector = new CarSelector();

        Car car = carSelector.getCar(RoadType.CITY);
        car.drive();
        car.stop();

        car = carSelector.getCar(RoadType.OFF_ROADS);
        car.drive();
        car.stop();

    }
}
