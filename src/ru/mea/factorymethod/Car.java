package ru.mea.factorymethod;

public interface Car {
    void drive();

    void stop();
}
