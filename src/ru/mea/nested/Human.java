package ru.mea.nested;

/**
 * /**
 *  * Пример вложенного класса-перечисления
 *  *
 *  * Вложенные классы, объявленные статическими,
 *  * называются статическими вложенными классами.
 *  * Nested classes that are declared static are called static nested classes.
 *  */
 @SuppressWarnings("unused")
public class Human {
    private String name;
    private int age;
    private Relations relations;

    /**
     * Вложенный enum по умолчанию static
     */
    enum Relations {
        SINGLE, //одинокий
        IN_LOVE, //влюбленный
        ACTIVE_SEARCH, //в активном поиске
        MARRIED, //в браке
        DIVORCED //в разводе
    }

    Human(String name, int age, Relations relations) {
        this.name = name;
        this.age = age;
        this.relations = relations;
    }

    public Relations getRelations() {
        return relations;
    }

    void setRelations(Relations relations) {
        this.relations = relations;
    }

    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", relations=" + relations +
                '}';
    }

}
