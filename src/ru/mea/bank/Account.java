package ru.mea.bank;

/**
 * Класс, демонстрирующий работу со счётом
 *
 * @author Е.А.Мешкова 18ИТ18
 */
public class Account {
    private String number;
    private String owner;
    private int amount;

    Account(String number, String owner) {
        this.number = number;
        this.owner = owner;
    }

    Account(String number, String owner, int amount) {
        this.number = number;
        this.owner = owner;
        this.amount = amount;
    }

    int getAmount() {
        return amount;
    }

    /**
     * Метод, возвращающий сумму снятую со счёта
     *
     * @param amountToWithdraw сумма снятия
     * @return сумму снятия с баланса
     */
    int withdraw(int amountToWithdraw) {
        if (amountToWithdraw < 0) {
            return 0;
        }
        if (amountToWithdraw > amount) {
            final int amountToReturn = amount;
            amount = 0;
            return amountToReturn;
        }
        amount = amount - amountToWithdraw;
        return amountToWithdraw;
    }

    /**
     * Метод, возвращающий сумму на которую пополнили счёт
     *
     * @param amountToDeposit сумма пополнения
     * @return сумму пополнения баланса
     */
    int replenish(int amountToDeposit) {
        if (amountToDeposit <= 0) {
            return 0;
        }
        amount += amountToDeposit;
        return amountToDeposit;
    }

    /**
     * Класс, демонстрирующий работу с картой
     *
     * @author Е.А.Мешкова 18ИТ18
     */
    class Card {
        private final String number;

        Card(String number) {
            this.number = number;
        }

        /**
         * Метод, демонстрирующий снятие денег
         *
         * @param amountToWithdraw сумма снятия
         * @return возвращает метод в Account
         */
        int withdraw(final int amountToWithdraw) {
            return Account.this.withdraw(amountToWithdraw);
        }

        /**
         * Метод, демонстрирующий снятие денег
         *
         * @param money сумма пополнения
         * @return возвращает метод в Account
         */
        int replenish(final int money) {
            return Account.this.replenish(money);
        }
    }
}
