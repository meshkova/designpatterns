package ru.mea.bank;


import java.util.Scanner;

/**
 * Класс, демонстрирующий работу банка
 *
 * @author Е.А.Мешкова 18ИТ18
 */
public class Demo {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        final Account accountSberbank = new Account("0987654321123456", "Meshkova Ekaterina");
        final Account accountVtb = new Account("1234567890123456", "Meshkova Ekaterina", 567);

        final Account.Card cardSberbankMastercard = accountSberbank.new Card("2332 45554 6776 8998");
        final Account.Card cardSberbankWorld = accountSberbank.new Card("1234 5678 2345 3456");
        final Account.Card cardVtbWorld = accountVtb.new Card("1221 2332 3443 4554");
        String str = "да";
        while (str.equalsIgnoreCase("да")) {
            System.out.println("Выберите банк: 1 - Sberbank, 2 - VTB");
            int choice = scanner.nextInt();
            switch (choice) {
                case 1:
                    System.out.println("Если вы хотите использовать карту cardSberbankMastercard  нажмите 1 " +
                            "если cardSberbankWorld нажмите 2");
                    int choiceCard = scanner.nextInt();
                    if (choiceCard < 1 || choiceCard > 2) return;
                    if (choiceCard == 1) chooseAct(cardSberbankMastercard, accountSberbank);
                    if (choiceCard == 2) chooseAct(cardSberbankWorld, accountSberbank);
                    break;
                case 2:
                    chooseAct(cardVtbWorld, accountVtb);
                    break;
                default:
                    System.out.println("Ввести можно только 1 либо 2");
                    break;
            }
            System.out.println("Что-то ещё?");
            str = scanner.next();
        }

    }


    /**
     * Метод, взаимнодействующий с пользователем и выполняющий проверку
     *
     * @param card    карта
     * @param account счёт
     */
    private static void checkingDeposits(Account.Card card, Account account) {
        System.out.println("Введите какую сумму вы хотите положить на карту.");
        int amountToDeposit = scanner.nextInt();
        int result = card.replenish(amountToDeposit);
        if (result == 0) {
            System.out.println("Сумма для пополнения некорректна.");
        }
        System.out.println("Баланс: " + account.getAmount());
    }

    /**
     * Метод, взаимнодействующий с пользователем и выполняющий проверку
     *
     * @param card    карта
     * @param account счёт
     */
    private static void checkingWithdrawals(Account.Card card, Account account) {
        System.out.println("Введите какую сумму вы хотите снять с карты");
        int amountToWithdraw = scanner.nextInt();
        int result = card.withdraw(amountToWithdraw);
        if (result == 0) {
            System.out.println("Введена некорректная сумма для снятия.");
        }
        if (result < amountToWithdraw) {
            System.out.println("Была введена сумма для снятия больше чем на балансе.");
        }
        System.out.println("Баланс: " + account.getAmount());
    }

    /**
     * Метод, предлагающий выбрать операцию
     *
     * @param card    карта
     * @param account счёт
     */
    private static void chooseAct(Account.Card card, Account account) {
        System.out.println("Если вы хотите положить деньги на карту нажмите 1, если снять нажмите 2, " +
                "если хотите прервать операцию, нажмите 3");
        int choice = scanner.nextInt();
        switch (choice) {
            case 1:
                checkingDeposits(card, account);
                break;
            case 2:
                checkingWithdrawals(card, account);
                break;
            case 3:
                System.out.println("Вы прервали операцию");
                break;
            default:
                System.out.println("Ввести можно только 1, 2 или 3");
                break;
        }

    }
}





