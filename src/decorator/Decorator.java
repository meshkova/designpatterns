package decorator;

public abstract class Decorator implements Dessert {
    private Dessert dessert;

    public Decorator(Dessert dessert) {
        this.dessert = dessert;
    }
    public abstract void withTopping();

    public void topping(){
        dessert.topping();
        withTopping();
    }
}
