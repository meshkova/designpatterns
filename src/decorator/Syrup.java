package decorator;

public class Syrup extends Decorator {
    public Syrup(Dessert dessert) {
        super(dessert);
    }

    @Override
    public void withTopping() {
        System.out.println("   добавлен сироп");
    }
}
