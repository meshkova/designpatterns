package decorator;

public class Powder extends Decorator {
    public Powder(Dessert dessert) {
        super(dessert);
    }

    @Override
    public void withTopping() {
        System.out.println("   добавлена присыпка");
    }
}
