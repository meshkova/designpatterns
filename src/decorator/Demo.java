package decorator;

import java.util.Random;

public class Demo {
    public static void main(String[] args) {
        Dessert iceCream;
        Dessert yougurt;
        Dessert smoothies;
        Random random = new Random();
        boolean sale = random.nextBoolean();
        if (sale) {
            sale = random.nextBoolean();
            if (sale) {
                sale = random.nextBoolean();
                if (sale) {
                    yougurt = new Powder(new Yogurt());
                } else {
                    yougurt = new Syrup(new Yogurt());
                }
            } else {
                yougurt = new Powder(new Syrup(new Yogurt()));
            }
        } else {
            yougurt = new Yogurt();
        }
        if (sale) {
            sale = random.nextBoolean();
            if (sale) {
                sale = random.nextBoolean();
                if (sale) {
                    iceCream = new Powder(new IceCream());
                } else {
                    iceCream = new Syrup(new IceCream());
                }
            } else {
                iceCream = new Powder(new Syrup(new IceCream()));
            }
        } else {
            iceCream = new IceCream();
        }
        if (sale) {
            sale = random.nextBoolean();
            if (sale) {
                sale = random.nextBoolean();
                if (sale) {
                    smoothies = new Powder(new Smoothies());
                } else {
                    smoothies = new Syrup(new Smoothies());
                }
            } else {
                smoothies = new Powder(new Syrup(new Smoothies()));
            }
        } else {
            smoothies = new Smoothies();
        }
        iceCream.topping();
        yougurt.topping();
        smoothies.topping();
    }

}
